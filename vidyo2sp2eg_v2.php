<?php 

require_once('sharepointAPI.php');
require_once('conf.php');

date_default_timezone_set('Europe/Zurich');

$first  = new DateTime();

// Sharepoint soap connection
$spClient = new SharePointAPI($AVC_LOGIN['user'], $AVC_LOGIN['pwd'], 'https://espace.cern.ch/AVC-workspace/videoconference/_vti_bin/Lists.asmx?wsdl');
$spClient->setReturnType('object');
$spList='Vidyo external accounts';

// Vidyo soap connection
$vidyo_url = "https://vidyoportal.cern.ch/services/VidyoPortalAdminService?wsdl";
$credentials = array('login' => $API_LOGIN['user'], 'password' => $API_LOGIN['pwd']);
$vidyoClient = new SoapClient($vidyo_url, $credentials);

// Egroups soap connection
$egroups_url = 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl';
$avc_login = array('login' => $AVC_LOGIN['user'], 'password' => $AVC_LOGIN['pwd']);
$egClient = new SoapClient($egroups_url, $avc_login);
$eGroup = 'vidyo-external-admins';

// Receives a string with emails and names, and returns and array with email => name
function parse_email($str){
	$emails = array();

	if(preg_match_all('/\s*"?([^><,"]+)"?\s*((?:<[^><,]+>)?)\s*/', $str, $matches, PREG_SET_ORDER) > 0){
    	foreach($matches as $m){
        	if(! empty($m[2])){
            	$emails[trim($m[2], '<>')] = $m[1];
        	} else {
            	$emails[$m[1]] = '';
            }
    	}
	}
	return $emails;
}


try {
 // use the method GetMembers to get all the members of Vidyo
 $result = $vidyoClient->GetMembers(array('query' => ''));
} catch (SoapFault $fault) {
 echo "Sorry, API GetMembers() returned the following ERROR: ".$fault->faultcode."-".$fault->faultstring.".";
}

// everything went fine, extract the needed data
$vidyo_members = $result->member;

$vidyo_external = array();

// Create an array with Vidyo external accounts
foreach ($vidyo_members as $member) {
	// get the group of the member
	$group = $member->groupName;
	// check if group is External
	if ($group == 'External') {
		$name = $member->displayName;
		$email = $member->name;
        $extension = $member->extension;
        $data = array('name'=>$name,'email'=>$email,'extension'=>$extension);
        array_push($vidyo_external, $data);
    }
}

// Extract items from SP list
$sp_items = $spClient->read($spList);

$sp_external = array();

// Create an array with SP list old items
foreach ($sp_items as $item) {
	$name = $item->title;
	$email = $item->affiliation;
	$extension = $item->vidyo_x0020_id;
	$data = array('name'=>$name,'email'=>$email,'extension'=>$extension);
    array_push($sp_external, $data);
}

// Check the difference between current vidyo external accounts and old sp list
$vidyo_sp = array_diff_assoc($sp_external, $vidyo_external);

// Remove old emails from egroup
if (!empty($vidyo_sp)){
	foreach ($vidyo_sp as $item) {
		$email = $item['email'];
		$egClient->removeEgroupEmailMembers(array('p_niceUserid' => $avc_user, 'p_password' => $avc_pwd, 
        	'p_egroupName' => $eGroup, 'p_members' => array('Member' => array('Email' => $email))));
	}
}

// Delete all items from Sharepoint List
foreach ($sp_items as $item) {
	$id = $item->id;
	$spClient->delete($spList, $id);
}

// Extract External Vidyo members and add them to SP and eGroups
foreach ($vidyo_external as $member) {
	$name = $member['name'];
	$email = $member['email'];
    $extension = $member['extension'];
    // Populate list with new items
    $spClient->write($spList, array('Title'=>$name,'Affiliation'=>$email,'Vidyo_x0020_id'=>$extension));
    // Add Members to eGroup List

    echo $eGroup . "\n";
    echo $email  . "\n";

    $egClient->addEgroupEmailMembers(array('p_niceUserid' => $AVC_LOGIN['user'], 'p_password' => $AVC_LOGIN['pwd'],
     	'egroupName' => $eGroup, 'emails' => array($email),
	    'overwriteMembers' => False));
}

// Vidyo Infra
$result = $spClient->read('Vidyo Infrastructure Elements');
$contacts = array();
$temp = array();

// Get emails from Vidyo Infra SP list
foreach ($result as $items) {
    if(isset($items->contact)){
    	$contact = $items->contact;
    	if(strpos($contact, '@')){
   		$temp = parse_email($contact);
        	$contacts += $temp;
    	}
    }
}

// Add Vidyo Infra emails to eGroup
foreach ($contacts as $email => $value) {
	$egClient->addEgroupEmailMembers(array('p_niceUserid' => $AVC_LOGIN['user'], 'p_password' => $AVC_LOGIN['pwd'], 
     	'egroupName' => $eGroup, 'emails' => array($email),
	    'overwriteMembers' => False));
}

$second = new DateTime();

$diff = $first->diff($second); 

echo "Vidyo external accounts to eGroups script done in: ".$diff->format('%S')." seconds"."\n";

unset($spClient);
unset($vidyoClient);
unset($egClient);

